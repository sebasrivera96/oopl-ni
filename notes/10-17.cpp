// -----------
// Thu, 15 Oct
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Notes
	these notes are on Canvas and GitLab
*/

/*
What did we do last time?

introductions
assertions
unit testing
coverage
isPrime
*/

/*
What will we do this time?

two tokens: *, &
two contexts: modifying a type, modifying a variable
pass by value, by address, by reference
incr
exceptions
*/

(cout << "hi") << "ramsey";
ostream& o = (cout << "hi");

int i = 2;
int v = i;
++v;
cout << i; // 2

int  i = 2;
int* p = i;   // no
int* p = &i;  // & takes the address of l-value
// ++p;
// cout << i; // 2
++*p;         // * deferences a pointer or anything that overloads *
cout << i;    // 3

int i;
i = 2;

int* p;
p = &i;
int j = 3;
p = &j;

int& r = &i;        // a reference, no
int& r = i;         // create an alias for i FOREVER
cout << (&r == &i); // true
int& s;             // no

void f (int v) {
    ++v;}

int main () {
    int i = 2;
    f(i);
    cout << i; // 2
    }

void g (int* p) {
    ++p;}

int main () {
    int j = 2;
    g(j);      // no
    g(&j);
    cout << j; // 2
    }

void g (int* p) {
    ++*p;}

int main () {
    int j = 2;
    g(j);      // no
    g(&j);
    g(185);    // no
    g(0);
    cout << j; // 3
    }

void h (int& r) {
    ++*r;         // no
    ++r;}

int main () {
    int k = 2;
    h(&k);     // no
    h(k);
    h(185);    // no
    h(0);      // no
    cout << k; // 3
    }

int i = 2; // initialization
i = 2;     // assigment

/*
1. references have cleaner, but more opaque syntax
2. pointers have to be paranoid about 0
3. certain mistakes cause different runtime behaviors with pointers, but don't compile with references
*/

int* a = new int[100];
...
delete [] a;
// tried to make use of a, now

/*
1. forget to delete
2. delete it more than once
3. delete the wrong address
4. use the pointer after delete
*/

/*
a good tool is Valgrind, which is a memory checker
*/

f(++i);
f(i++);

int& pre_incr (int& r) {
    return r += 1;}

int post_incr (int& r) {
    int j = r;
    r += 1;
    return j;}

/*
assertions are not good user errors
exceptions are
*/

/*
pretend that we don't have exceptions
*/

// using the return

int f (...) {
    ...
    if (<something wrong>)
        return <special value>
    ...

void g (...) {
    ...
    int i = f(...);
    if (i = <special value>)
        <something wrong>
    ...}

// using a global

int h = 0;

int f (...) {
    h = 0;
    ...
    if (<something wrong>) {
        h = <special value>
        return ...}
    ...

void g (...) {
    ...
    int i = f(...);
    if (h == <special value>)
        <something wrong>
    ...}

// using a parameter

int f (..., int& e2) {
    ...
    if (<something wrong>) {
        e2 = <special value>
        return ...}
    ...

void g (...) {
    ...
    int e = 0;
    int i = f(..., e);
    if (e == <special value>)
        <something wrong>
    ...}

// using exceptions

int f (...) {
    ...
    if (<something wrong>)
        throw Tiger(...);  // throw always makes a copy
    ...}

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (Mammal e) {     // never catch value
        <something wrong>}
   ...}

...
g(...)
...

int f (...) {
    ...
    if (<something wrong>)
        Tiger x;
        throw &x;

    ...}

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (Mammal* e) {     // never catch by address
        <something wrong>}
   ...}

...
g(...)
...

int f (...) {
    ...
    if (<something wrong>)
        throw Tiger(...);

    ...}

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (Mammal& e) {     // always catch by reference
        <something wrong>}
   ...}

...
g(...)
...

int f (...) {
    ...
    if (<something wrong>)
        throw Tiger(...);
    ...}

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (Tiger& e) {
        ...
        throw Mammal(...);} // has to go to the calling try block
    catch (Mammal& e) {
        <something wrong>}
   ...}

...
g(...)
...

int f (...) {
    ...
    if (<something wrong>)
        throw Tiger(...);
    ...}

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (Tiger& e) {
        ...
        throw Tiger(...);}
    catch (Mammal& e) {
        <something wrong>}
   ...}

...
g(...)
...

int f (...) {
    ...
    if (<something wrong>)
        throw Tiger(...);
    ...}

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (Tiger& e) {
        ...
        throw e;}
    catch (Mammal& e) {
        <something wrong>}
   ...}

...
g(...)
...

int f (...) {
    ...
    if (<something wrong>)
        throw Tiger(...);
    ...}

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (...) { // special ...
        ...
        throw;}
    catch (Mammal& e) {
        <something wrong>}
   ...}

...
g(...)
...

string s = "abc";
string t = "abc";
cout << (s == t); // true

cout << (s == "abc"); // true

void f (string s) {
    ...}

int main () {
    string t = "abc";
    f(t);

    f("abc");
