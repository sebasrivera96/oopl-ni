// -----------
// Tue,  5 Nov
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Notes
	these notes are on Canvas and GitLab
*/

/*
What did we do last time?

heap arrays
unique_ptr
vector
iterators
*/

/*
What will we do this time?

range iterator
*/

vector<A>           x(10, A(2));
vector<A>::iterator b = begin(x);
assert(x.size() == 10);
cout << *b << " ";
x.push_back(A(3));
cout << *b << endl;}

for (int i = 0; i != 10; ++i)
    s += i;

int s = 0;
{
int i = 0;
while (i != 10) {
    s += i;
    ++i;}
}

for (T v : a) {
    s += v;}

/*
what can you conclude about a
*/

int s = 0;
T* b = begin(a);
T* e = end(a);
while (b != e) {
    T v = *b;
    s += v;
    ++b;}

/*
input iterator
    * (read only)
    ==, !=
    ++ (pre and post)
*/

struct A {};

A()
A(const A&)
~A()
=(const A&)

template <typename T>
class RangeIterator {
    private:
        T _v;

    public:
//        RangeIterator (T v) {
//            _v = v;

        RangeIterator (const T& v) : _v (v)
            {}

//        const T& operator* () const {
//            return _v;

        T operator* () const {
            return _v;

        // b.operator==(e)
        bool operator== (const RangeIterator& rhs) const {
            return _v == rhs._v;}

        bool operator!= (const RangeIterator& rhs) const {
            return !(*this == rhs);

        RangeIterator& operator++ () { // pre
            ++_v;
            return *this;}

        RangeIteartor operator++ (int) { // post
            // RangeIterator* const this;
            RangeIterator tmp(*this);
            ++*this;
            return tmp;}
