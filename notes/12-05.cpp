// -----------
// Thu,  5 Dec
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Notes
	these notes are on Canvas and GitLab
*/

/*
What did we do last time?

allocators
shapes
*/

/*
What will we do this time?


*/

void test1 () {
    vector<A> x(2, B());
    ...}


struct A {
    void f (int) {}}

struct B : A {
    void f (int) {}}

int main () {
    B x;       // A() B()
    x.f(2);    // B::f
    return 0;}



struct A {
    void f (int) {}}

struct B : A {
    void g (int) {}}

int main () {
    B x;       // A() B()
    x.f(2);    // A::f
    return 0;}



struct A {
    void f (int) {}}

struct B : A {
    void f (string) {}}

int main () {
    B x;        // A() B()
    x.f("abc"); // B::f
    x.f(2);     // no compile
    return 0;}



struct A {
    void f (int) {}}

struct B : A {
    void f (string) {}}

int main () {
    B x;        // A() B()
    x.f("abc"); // B::f
    x.A::f(2);  // A::f
    return 0;}



struct A {
    void f (int) {}}

struct B : A {
    void f (int i)  {A::f(i);}
    void f (string) {}}

int main () {
    B x;        // A() B()
    x.f("abc"); // B::f
    x.f(2)      // A::f
    return 0;}



struct A {
    void f (int) {}}

struct B : A {
    using A::f;         // goes all the way up
    void f (string) {}}

int main () {
    B x;        // A() B()
    x.f("abc"); // B::f
    x.f(2)      // A::f
    return 0;}

Shape
    area
    move

Circle
    area
    radius



struct A {
    void f (int) {}}

struct B : A {
    void f (int) {}}

int main () {
    B x;
    x.f(2);    // B::f

    A* p = &x;
    *p.f(2);   // no
    (*p).f(2); // A::f
    p->f(2);   // A::f, static binding

    A y;
    y.f(2);    // A::f
    return 0;}



struct A {
    virtual void f (int) {}}

struct B : A {
    void f (int) {}}

int main () {
    B x;
    x.f(2);    // B::f

    A* p = &x;
    p->f(2);   // B::f, dynamic binding

    A y;
    x.f(2);    // A::f

    A* q = &y;
    q->f(2);   // A::f
    return 0;}



struct A {
    virtual void f (int) {}}

struct B : A {
    void f (int) {}}

int main () {
    A* p;

    if (...)
        p = new A(...);
    else
        p = new B(...);

    p->f(2);            // the right one

    A x;
    x.f(2);             // statically bound

    p->A::f(2);         // statically bound

    return 0;}

/*
virtual methods don't act dynamically:
    1. on objects
    2. with the scope operator
    3. in constructors
    4. in destructors
*/

/*
compile-time casts
    1. static_cast
    2. reinterpret_cast
    3. const_cast

run-time cast
    1. dynamic_cast (only defined with virtual)
*/

class Shape {
    friend bool operator == (const Shape& lhs, const Shape& rhs) {
        return lhs.equals(rhs);}
    ...

    virtual bool equals (const Shape& rhs) {
        ...}

    virtual Shape& operator = (const Shape& rhs) = default;

class Circle : public Shape {
    bool equals (const Shape& rhs) {
        ...}

    Circle& operator = (const Circle&) = default;
