// -----------
// Tue, 22 Oct
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Notes
	these notes are on Canvas and GitLab
*/

/*
What did we do last time?

two tokens: *, &
two contexts: modifying a type, modifying a variable
pass by value, by address, by reference
incr
exceptions
*/

/*
What will we do this time?

strcmp
operators
consts
types
*/

int  i = 2;
int& r = i;
int  j = r;

void f (string t) {
    ...}

int main () {
    string s = "abc";
    f(s);
    f(string("abc"));
    f("abc");

assert(strcmp(e.what(), "abc") == 0);

int strcmp (const char* s, const char* t) {
    ...}

// read-write pointer, many-location pointer

const int ci;     // no
const int ci = 2;
++ci;             // no

int  i = 2;
int* p;
p = &i;
p = &ci;    // no

// read-only pointer, many-location pointer

int        i  = 2;
const int  ci = 3;
const int  cj = 4;
const int* p;
p = &ci;
++*p;              // no
p = &cj;
p = &i;
cout << *p;
...
cout << *p;

// read-write pointer, one-location pointer

int        i  = 2;
const int  ci = 3;
const int  cj = 4;
int* const cp;       // no
int* const cp = &ci; // no
int* const cp = &i;
++cp;                // no
++*cp;

int* const a = new int[100];
++*a;
++a;                         // no
delete [] a;

// read-only pointer, one-location pointer

int        i  = 2;
const int  ci = 3;
const int  cj = 4;
const int* const cpc;       // no
const int* const cpc = &ci;
const int* const cqc = &i;
++cpc;                      // no
++*cpc;                     // no

// read-write reference; T& === T* const

int        i  = 2;
const int  ci = 3;
const int  cj = 4;
int& r;            // no
int& r = ci;       // no
int& r = i;
r = ci;

// read-only reference; const T& === const T* const

int        i  = 2;
const int  ci = 3;
const int  cj = 4;
const int& r;      // no
const int& r = ci;
const int& s = i;
++r;               // no
++s;               // no


/*
wordier casting system

four casts
    1. const_cast
    2. static_cast
    3. reinterpret_cast
    4. dynamic_cast
*/

/*
standard template library (STL)
containers:
    1. vector
    2. list
    3. deque
*/

/*
vector, front-loaded array
cost of adding to the back:             amortized constant
cost of adding to the front/middle:     linear
cost of removing from the back:         constant
cost of removing from the front/middle: linear
cost of indexing:                       constant
*/

vector<int> x(10, 2);
cout << x.size();     // 10
cout << x.capacity(); // 10
cout << x[3];
x.push_back(5);       // linear
x.push_front(6);      // no

/*
list, doubly-linked list
cost of adding:   constant
cost of removing: constant
cost of indexing: N/A
*/

list<int> x(10, 2);
cout << x[3];       // no
x.push_back(5);     // constant
x.push_front(6);    // constant

vector<int> x(10, 2);
int* p = &x[3];
x.push_back(4);       // linear
cout << *p;           // undefined

/*
deque, middle-loaded array
cost of adding to the back:             amortized constant
cost of adding to the front/middle:     linear
cost of removing from the back:         constant
cost of removing from the front/middle: linear
cost of indexing:                       constant
*/

deque<int> x(10, 2);
x.push_back(5);     // constant, more expensive than vectors, +, /, and %
x.push_front(6);    // constant, more expensive than vectors, +, /, and %

deque<int> x(10, 2);  // assuming inner arrays are size 10
int* p = &x[3];
x.push_back(4);       // linear, way cheaper than vectors
cout << *p;           // fine
