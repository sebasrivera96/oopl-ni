// -----------
// Tue, 29 Oct
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Notes
	these notes are on Canvas and GitLab
*/

/*
What did we do last time?

container adapters
*/

/*
What will we do this time?

arrays
iterators
*/

int a[] = {2, 3, 4}; // int* const a
T   a[] = {2, 3, 4}; // T(int), 3 times

const int a[s] = {2, 3, 4};
const T   a[s] = {2, 3, 4}; // T(int), 3 times; T(), s - 3 times

int a[s]; // O(1)
T   a[s]; // O(n); T(), s times

const int a[s] = {}; // O(n), zeros
const T   a[s] = {}; // O(n); T(), s times


bool equal (int* b, int* e, int* x) {
    ...}

bool equal (long* b, long* e, long* x) {
    ...}

template <typename T1, typename T2>
bool equal (T1 b, T1 e, T2 x) {
    ...}

int a[] = {2, 3, 4};
int b[] = {2, 3, 4};

assert(equal(a, a + 3, b)); // T1, T2 -> int*

long a[] = {2, 3, 4};
long b[] = {2, 3, 4};

assert(equal(a, a + 3, b)); // T1, T2 -> long*

int  a[] = {2, 3, 4};
long b[] = {2, 3, 4};

assert(equal(a, a + 3, b)); // T1 -> int*, T2 -> long*

template <typename T1, typename T2>
bool equal (T1 b, T1 e, T2 x) {
    ...}

vector<int> s = {2, 3, 4};
vector<int  t = {2, 3, 4};

vector<int>::iterator b = s.begin();
vector<int>::iterator e = s.end();
vector<int>::iterator x = t.begin();

cout << *b;      // 2
++b;
cout << *b;      // 3
cout << (b == e) // false

assert(equal(s,         s + 3,   t));         // no
assert(equal(s.begin(), s.end(), t,begin())); // T1 -> vector<int>::iterator; T2 -> vector<int>::iterator
assert(equal(b,         e,       x));         // T1 -> vector<int>::iterator; T2 -> vector<int>::iterator

template <typename T>
class vector {
    private:
        ...

    public:
        class iterator {
            private:
                iterator (...) {...}

            public:

            ...};

        class const_iterator {
            private:
                iterator (...) {...}

            public:

            ...};

        vector (...) {...}

        iterator begin () {
            return iterator(...);}

        const_iterator begin () const {
            return iterator(...);}

int main () {
    const vector<int>     x(10, 2);
    vector<int>::iterator b = x.begin();
    cout << *b;                          // 2
    *b = 3;                              // no
    cout << *b;                          // 2

    int  i = 2;
    int* p = &i;
    cout << i;  // 2
    cout << *p; // 2

    i = 3;
    cout << i;  // 3
    cout << *p; // 3

    *p = 4;
    cout << i;  // 4
    cout << *p; // 4

int a[] = {...};
int b[] = {...};

equal(a + 5, a + 10, b + 15)

/*
what is the smallest that a can be: 10
what is the smallest that b can be: 20
*/

int i = 2;
int j = i;

T x = ...;
T y = x;
