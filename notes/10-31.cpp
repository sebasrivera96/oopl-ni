// -----------
// Thu, 31 Oct
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Notes
	these notes are on Canvas and GitLab
*/

/*
What did we do last time?

arrays
iterators
*/

/*
What will we do this time?

heap arrays
unique_ptr
vector
iterators
*/

int* const a = new int[s]; // O(1)
++a;                       // no
delete [] a;

T* const a = new T[s]; // O(n), T(),  s times
delete [] a;           // O(n), ~T(), s times

/*
in C, casting is done with ()
*/

/*
always defined, compile time casts
const_cast
static_cast
reinterpret_cast, most like a C cast

only sometimes define, run time cast
dynamic_cast
*/

T* a = new T[s];
...
delete [] a;

/*
1. delete wrong address
2. delete with the wrong step size
3. delete more than once
4. forget to delete
5. misuse the []
6. use the pointer after the delete
*/

/*
1. garbage collector
2. memory checker
3. do new/delete in the constructors/destructors of a class
*/

int* a = new int[s]; // O(1)
fill(a, a + s, v);   // O(n)

T* b = new T[s];     // O(n), T(),  s times
fill(b, b + s, v);   // O(n), =(T), s times

/*
under the hood vector uses an allocator
*/

vector<int> x(s, v); // O(n)
vector<T>   y(s, v); // O(n), T(T), s times

T x = ...;
T y = x;   // T(T)

{
vector<T> x(s, v); // T(T), s times
}                  // ~T(), s times

vector<T> x(10, v); // T(T), 10 times
vector<T> y( 5, w); // T(T),  5 times
x = y;              // =(T),  5 times, ~T(), 5 times

vector<T> x(10, v); // T(T), 10 times
vector<T> y(10, w); // T(T), 10 times
x = y;              // =(T), 10 times

vector<T> x( 5, v); // T(T),  5 times
vector<T> y(10, w); // T(T), 10 times
x = y;              // ~T(),  5 times, T(T), 10 times

/*
what properties did equal's II1 iterator need:
1. * (read only)
2. !=, ==
3. ++ (two: pre and post)
we're going to name this an input iterator
*/

/*
what properties did copy's OI iterator need:
1. * (write only)
2. ++ (two: pre and post)
we're going to name this an output iterator
*/

/*
what properties did fill's FI iterator need:
1. * (read/write only)
2. !=, ==
3. ++ (two: pre and post)
we're going to name this an forward iterator (<input>, write)
*/

/*
1. * (read/write only)
2. !=, ==
3. ++ (two: pre and post)
4. -- (two: pre and post)
we're going to name this an bidirectional iterator (<forward>, --)
*/

/*
1. * (read/write only)
2. !=, ==
3. ++ (two: pre and post)
4. -- (two: pre and post)
5. []
6. -
7. +/- int
8. <, <=, >, >
we're going to name this an random access iterator (<bidirectional>, ...)
*/

/*
when you write an algorithm
depend on the weakest possible iterator
*/

/*
when you write a container
provide the strongest possible iterator
*/

/*
input
output
forward
bidirectional
random access
*/

/*
Java library doesn't have this behavior
*/

interface List {...}
    get(int) -> T
class ArrayList  implements List, cost of get(): O(1)
class LinkedList implements List, cost of get(): O(n)
class DequeList  implements List, cost of get(): O(1)
...
