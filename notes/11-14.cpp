// -----------
// Thu, 14 Nov
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Notes
	these notes are on Canvas and GitLab
*/

/*
What did we do last time?

factorial
accumulate
functions
transform
lambdas
*/

/*
What will we do this time?

initialization
initizlizer_list
vector
*/

A x(2);
A x = 2; // not, if explicit

A x(2, 3, 4);
A x(2, 3);
A x(2);
A x();        // NOT constructing an A
              // declaring a function, named x, that takes no arguments, and returns an A
A x;

A x(int);
A x(int, double);
A x();

template <typename T>
class vector {
    private:
//      T*  _a;
//      int _s;
        T*  _b;
        T*  _e;
    public:
        vector () {
            _b = nullptr;
            _e = nullptr;}
        vector (int s) {
            _b = new T[s];
            _e = _b + s;
            fill(_b, _e, T());}
        vector (int s, const T& v) {
            _b = new T[s];
            _e = _b + s;
            fill(_b, _e, v);}
        vector (initializer_list<T> rhs) {
            _b = new T[rhs.size()];
            _e = _b + rhs.size();
            copy(begin(rhs), end(rhs), _b);}
        T& operator [] (int i) {
            return _b[i];}
        const T& operator [] (int) const {
            return (*const_cast<vector*>(this))[i];}
        int size () const {
            return _e - _b;}
        T* begin () {
            return _b;}
        const T* begin () const {
            return _b;}
        T* end () {
            return _e;}
        const T* end () const {
            return _e;}

vector<int> x(...);
int* p = x.begin();
(*x.begin()) = ...;

const vector<int> x(...);
const int* p = x.begin();
(*x.begin()) = ...;       // no

struct A {};

/*
A()
A(A)
=(A)
~A()
*/

vector<int> x(10, 2);
vector<int> y(x);
