// -----------
// Thu, 21 Nov
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Notes
	these notes are on Canvas and GitLab
*/

/*
What did we do last time?

copy constructor
copy assignment
equality
r-value reference
*/

/*
What will we do this time?

move semantics
allocators
*/

// my_vector& operator = (my_vector&& rhs) {
my_vector& operator = (my_vector   rhs) {

vector<int> f (...) {
    ...}

int main () {
    vector<int> x = f(...);
    return 0;}

/*
constructors exhibit refinement overriding
destructors  exhibit refinement overriding
*/

B<int> y = x;  // A(const A&) B(const B&);

/*
move() is really a cast to an r-value
*/

C (C&& rhs) {           // A()
    cout << "C(C&&) ";}

C (C&& rhs) :           // A(A)
    _x (rhs._x) {
    cout << "C(C&&) ";}

C<int> y = x;       // A(A) C(C)

C<int> y = move(x); // A(A&&) C(C&&)

C<int> y = move(x); // A(A) C(C&&)    // no

explicit my_vector (size_type s) {
    if (s != 0) {
        _b = new value_type[s];
        _e = _b + s;
        fill(begin(), end(), value_type());}}

vector<T> x(10); // T(), s times; T(), once; =(T), s times; ~T()

my_vector (size_type s, const_reference v) {
    if (s != 0) {
        _b = new value_type[s];
        _e = _b + s;
        fill(begin(), end(), v);}}

T v(...);
vector<T> x(s, v); // T(), s times; =(T) s times

// real vector

vector<T> x(s, v); // T(T), s times

/*
allocators
*/

#include <memory>

/*
allocate
construct
destroy
deallocate
*/

T  v(...);
T* a = new T[s];      // allocate enough room for s Ts; T(),  s times
fill(a, a + s, v);    // =(T), s times
...
delete [] a;          // ~T(), s times; deallocating the space

allocator<T> x;
T* a = x.allocate(s);  // <nothing>, like malloc()

//x.construct(a,     v); // T(T)
//x.construct(a + 1, v);
//...

my_unitialized_fill(x, a, a + s, v); // T(T), s times
...
// delete [] a;                      // undefined
//x.destroy(a);
//x.destroy(a + 1);
//...
my_destroy(x, a, a + s);             // ~T(), s times
x.deallocate(a);                  // <nothing>, like free()

template <typename T, typename A = allocator<T>> // policy design pattern
class my_vector {
    private:
        A       _a;
        pointer _b = nullptr;
        pointer _e = nullptr;

    public:
//        explicit my_vector (size_type s) {
//            if (s != 0) {
//                _b = new value_type[s];
//                _e = _b + s;
//                fill(begin(), end(), value_type());}}

        explicit my_vector (size_type s, const A& a = A()) :
            _a (a) {
            if (s != 0) {
                _b = _a.allocate(s);
                _e = _b + s;
                uninitialized_fill(x, begin(), end(), value_type());}}

//        ~my_vector () {
//            delete [] _b;}

        ~my_vector () {
            destroy(_a, _b, _e);
            _a.deallocate(_b);}

int main () {
    vector<int>                      x(10);
    josh_allocator<int> a(...);
    vector<int, josh_allocator<int>> x(10, a);
    return 0;}
