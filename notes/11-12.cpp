// -----------
// Tue, 12 Nov
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Notes
	these notes are on Canvas and GitLab
*/

/*
What did we do last time?

range
implicit type conversion
symmetric operators
friends
compiler optimizations
*/

/*
What will we do this time?

factorial
accumulate
functions
transform
lambdas
*/

x.operator==(2)

A (const T& v) { // T()
    _v = v;}     // =(T)

operator==(x, 2)

/*
factorial
    1. iteratively
    2. recursively
*/

/*
input (II)
output (OI)
forward (FI)
bidirectional (BI)
random access (RI)
*/

template <typename II, typename T, typename BF>
accumulate (II b, II e, const T& v, BF f) {
    f(x, y)

/*
v <bf> a0 <bf> a1 <bf> a2 <bf> ... <bf> an-1

bf = +
v  = 0
sum

bf = *
v  = 1
product

bf = *
v  = 1
as = 1..n
factorial
*/

struct A {};

/*
A()
=(A)
A(A)
~A()
*/

int i = my_multiplies_1(2, 3) // T -> int
cout << i;                    // 6

int i = my_multiplies_2(2, 3) // no

my_multiplies_2 x;         // my_multiples_2()
int i = x(2, 3)
int i = x.operator()(2, 3)
cout << i;                 // 6

template <typename II, typename T, typename BF>
accumulate (II b, II e, const T& v, BF f) {
    f(x, y) -> MAYBE f.operator()(x, y)

template <typename T>
T my_multiplies_1 (const T& x, const T& y) {
    return x * y;}

int factorial_accumulate_1 (int n) {
    assert(n >= 0);
    vector<int>        x(n);
    RangeIterator<int> b = 1;
    RangeIterator<int> e = n + 1;
    auto               f = my_multiplies_1<int>;
    const int v = accumulate(b, e, 1, f);
    return v;}

int (*f) (int, int) = my_multiples_1<int>

int v = accumulate(b, e, 1, my_multiplies_1<int>);   // II -> RI<int>, T -> int, BF -> int (*) (int, int)

template <typename T>
T my_plus_1 (const T& x, const T& y) {
    return x + y;}

int v = accumulate(b, e, 1, my_plus_1<int>);   // II -> RI<int>, T -> int, BF -> int (*) (int, int)

template <typename T>
struct my_multiplies_2 {
    T operator () (const T& x, const T& y) const {
        return x * y;}};

int factorial_accumulate_2 (int n) {
    assert(n >= 0);
    vector<int>        x(n);
    RangeIterator<int> b = 1;
    RangeIterator<int> e = n + 1;
    auto               f = my_multiplies_2<int>();
    const int v = accumulate(b, e, 1, f);
    return v;}

my_multiples_2 f = my_multiplies_2<int>()

int v = accumulate(b, e, 1, my_multiplies_2<int>);   // no
int v = accumulate(b, e, 1, my_multiplies_2<int>()); // II -> RI<int>, T -> int, BF -> my_multiples_2

template <typename T>
struct my_plus_2 {
    T operator () (const T& x, const T& y) const {
        return x + y;}};

int v = accumulate(b, e, 1, my_plus_2<int>()); // II -> RI<int>, T -> int, BF -> my_plus_2

/*
function objects
    1. more efficient inlining
    2. a function with memory
*/

int (*f) (int)        = square1;
function<int (int)> f = square1; // this is an expensive function type


int (*f) (int) = square2<int>;

<no> f = [] (int x) -> int {return pow(x, 2);};

function<int (int)> f = [] (int x) -> int {return pow(x, 2);}; // this an expensive function type
