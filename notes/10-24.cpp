// -----------
// Thu, 24 Oct
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Notes
	these notes are on Canvas and GitLab
*/

/*
What did we do last time?

strcmp
operators
consts
types
containers
*/

/*
What will we do this time?

container adapters
*/

/*
stack
    1. push (push_back)
    2. pop (pop_back)
    3. top (back, returns T& and const T&)
    4. size
    5. empty
*/

/*
stack could adapt
    1. vector
    2. list
    3. deque (default)

1. stack could inherit a backing container
2. stack could contain a backing container instance

stack SHOULD contain
stack SHOULD use the back, to accommodate vector
stack SHOULD default to using
*/

// reusing implementation and interface

template <typename T, typename C>
class stack1 : public C {         // get some methods with no effort
    public:
        void push (const T& v) {
            push_back(v);}

// reusing implementation

template <typename T, typename C = deque<T>>
class stack2 {
    private:
        C _c;

    public:
        stack () {
            ???}

        void push (const T& v) {
            _c.push_back(v);}

        const T& top () const {
            ...}

        T& top () {
            ...}

        int size () const {
            ...}};

void f (const stack& s) {
    cout << s.top();
    s.top() = 3;          // no
    cout << s.size();

int main () {
    stack<int, vector<int>> x;
    stack<int>              y;
    ...fill x in...
    cout << x.top();
    x.top() = 2;
    f(x);
    cout << x.size();

/*
queue (mapped to the front and the back)
    1. push (push_back)
    2. pop (pop_front)
    3. front (two of them)
    4. back (two of them)
    5. size
    6. empty
*/

/*
queue could adapt
    1. NOT vector
    2. list
    3. deque (default)
*/

/*
priority_queue
    1. push
    2. pop
    3. top (one of them, const T&)
    4. size
    5. empty

    6. needs its element to have < defined
*/

/*
priority queue could adapt
    1. vector (default)
    2. NOT list
    3. deque
*/

/*
set (binary tree)
    1. not duplicates
    2. needs its element to have < defined
*/

/*
unordered set (hash table, an array of linked lists)
    1. not duplicates
    2. needs it elements to be hashable
*/

/*
map (binary tree)
    1. not duplicate keys
    2. needs its keys to have < defined
*/

/*
unordered map (hash table, an array of linked lists)
    1. not duplicate keys
    2. needs its keys to be hashable
*/

class B {
    ...}

struct A {
    int _i;
    B   _x;

    void f () const {
        ...}          // const A* const this

    // let's define what I know I already get for free

    A () :       // member initialization list
        _i (0),
        _x (...)
        {}

    A (const A& rhs) :
        _i (rhs._i),
        _x (rhs._x)
        {}

    A& operator = (const A& rhs) {
        _i = rhs._i;
        _x = rhs._x;
        return *this;              // A* const this;

    ~A () {
        {}

void f (A z) {
    ...}

int main () {
    A x;         // default constructor
    cout << x.i; // ?

    A x = y;     // copy constructor
    A x(y);

    f(x);

    x = y;
    x.operator=(y);
