// -----------
// Tue,  3 Dec
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Notes
	these notes are on Canvas and GitLab
*/

/*
What did we do last time?

move semantics
allocators
*/

/*
What will we do this time?

allocators
shapes
*/

T* a = new T[s];   // built-in type: O(1); user type: T(),  s times, O(s)
fill(a, a + s, v); // built-in type: O(s); user type: =(T), s times, O(s)
...
delete [] a;       // built-in type: O(1); user type: ~T(), s times, O(s)

allocator<T> x;              // O(1)
T* a = x.allocate(s);        // O(1)
for (int i = 0; i != s; ++i)
    x.construct(a + i, v)    // built-in type: O(s); user type: T(T), s times, O(s)
...
for (int i = 0; i != s; ++i)
    x.destroy(a + i)
x.deallocate(a, s)

int i;
cin  >> i; // cin.operator>>(i)
cout << i; // cout.operator<<(i)

Shape s;
cin  >> s; // operator>>(cin, s)
cout << s; // operator<<(cout, s)

Shape
    area
    move

Circle
    area
    radius

class Circle : Shape
// same as
class Circle : private Shape

struct Circle : Shape
// same as
struct Circle : public Shape

Shape* p = new Circle(...) // only possible if the inheritance is public

stack<int> x;
stack<int, vector<int>> y;
stack<int, deque<int>>  z;

class B {
   A _x}

class B : A {
    ...}

Shape* const p = new Circle(2, 3, 4);
*p.move(4, 5);                        // no
(*p).move(4, 5);
p->move(4, 5);

T* p = ...
p[i]       // go forward i * sizeof(T)

class Shape {
    public:
        int _x;
        int _y;
    ...};

class Circle : public Shape {
    public:
        int _r;

        Circle (int x, int y, int r) : // no
                _x (x),
                _y (y),
                _r (r)
            {}

        Circle (int x, int y, int r) : // no
                Shape (),              // there is no default constructor
                _r (r) {
            _x = x;
            _y = y;}
