// -----------
// Thu, 12 Dec
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Notes
	these notes are on Canvas and GitLab
*/

/*
What did we do last time?

virtual method overriding
pure virtual methods
abstract classes
protected copy assignment
covariant return types
*/

/*
What will we do this time?


*/

class Shape2 {
    private:
        AbstractShape* _p;

    public:
        AbstractShape& operator * () {
            ...}

        AbstractShape* operator -> () {
            ...}};

Shape2 x = new Circle(...);
cout << *x.area();            // no
cout << (*x).area();
cout << x.operator*().area();

cout << x->area();
cout << x.operator->()->area();

AbstractShape* p = new Circle(...);
cout << (*p).area();
cout << p->area();

vector<Shape> x(s, new Circle(...));
/*
    Circle(...), once
    Shape(AbstractShape*), once
    Shape(Shape), s times
    ~Shape(), once
    ~Circle()
*/
...

vector<AbstractShape*> x(s, new Circle(...));
/*
    Circle(...), once
*/
