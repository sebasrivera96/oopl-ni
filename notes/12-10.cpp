// -----------
// Tue, 10 Dec
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Notes
	these notes are on Canvas and GitLab
*/

/*
What did we do last time?

method overriding
virtual
*/

/*
What will we do this time?


virtual method overriding
pure virtual methods
abstract classes
protected copy assignment
covariant return types
*/

// an object
B x;
x.f();

// scope operator
p->A::f()

// construction

// destruction

struct A {
    virtual void f (int) {...}};

struct B : A {
    void f (int) {...}};

int main () {
    A* p = new B;
    p->f(2);      // B::f
    return 0;}



struct A {
    virtual void f (int) {...}};

struct B : A {
    void g (int) {...}};

int main () {
    A* p = new B;
    p->f(2);      // A::f
    return 0;}



struct A {
    virtual void f (long) {...}};

struct B : A {
    void f (int) {...}};

int main () {
    A* p = new B;
    p->f(2);      // A::f
    return 0;}

/*
when overriding a virtual method, you have to respect five properties
    1. the name
    2. the return type must be covariant
    3. the argument types
    4. the number of arguments
    5. whether the method is const or not
*/

struct A {
    virtual void f (long) {...}};

struct B : A {
    void f (int) override {...}}; // no

int main () {
    A* p = new B;
    p->f(2);      // A::f
    return 0;}

/*
abstract class is a class that you can NOT instantiate
*/

// one reason to want an abstract class

struct A {}

struct B : A {}

/*
to change only B   behaviors, edit B
to change only A&B behaviors, edit A
to change only A   behaviors, can't
*/

struct A' {}

struct A : A' {}
struct B : A' {}

/*
to change only B   behaviors, edit B
to change only A&B behaviors, edit A'
to change only A   behaviors, edit A
*/

// another reason to want an abstract class

class Shape {...};

class Circle : public Shape {...};

class Triangle : public Shape {
    public:
        Triangle (...) {...}};

/*
T()
T(T)
=(T)
~T()
*/

int main () {
    Shape* p = new Circle(...);
    cout << p->area();          // pi r squared

    p = new Triangle(...);
    cout << p->area();          // 0
    return 0;}

class Shape {
    public:
        virtual double area () = 0;
    ...};

struct A {
    virtual void f (int) = 0;}; // pure virtual method

/*
consequences of a pure virtual method
    1. makes the class abstract
    2. children have to define the method OR becomes abstract
    3. optional to define the base class method
*/

struct A {
    virtual void f (int) = 0;};

struct B : A {
    void f (int) {...}};

int main () {
    A* p = new B;
    p->f(2);      // B::f
    return 0;}



struct A {
    virtual void f (long) = 0;};

struct B : A {
    void f (int) override {...}};

int main () {
    A* p = new B; // no
    p->f(2);      // B::f
    return 0;}




struct A {
    virtual void f (int) final {...};

struct B : A {

int main () {
    A* p = new B;
    p->f(2);      // A::f
    return 0;}

/*
consequences of a final method
    1. children can not override
*/

struct A {
    void f (int) {}
    virtual g (int) {}
    virtual h (int) = 0;
    virtual m (int) final;};

/*
consequences to the children:
    f: optional definition, but unusual because it doesn't behave well <bad>
    g: optional definition, behaves ok, but causes change in runtime behavior, with changes in signatures <bad>
    h: required definition, or it becomes abstract <good>
    m: prohibited defintion <good>
*/

class Shape {
    protected:
        bool equals (...) const = 0;
    ...};

bool Shape::equals (...) const {...}

struct A {
    virtual ~A () = 0;};

A::~A() {}

/*
consequences of a pure virtual destructor
    1. makes the class abstract
    2.
    3.
*/

/*
you never bother with a pure virtual destructor, unless you don't have any other pure virtual methods
*/

AbstractShape* p = new Circle(...);
AbstractShape* q = p->clone();

Circle* p = new Circle(...);
Circle* q = p->clone();      // no, yes with covariant return type for clone()

Circle* p = new Circle(...);
AbstractShape* q = p->clone();

vector<Circle> x(s, Circle(...)); Circle(...), once; Circle(Circle), s times; ~Circle(), once

vector<AbstractShape>> x(s, Circle(...)); // no

vector<AbstractShape*> x(s, Circle(...)); // no

vector<AbstractShape*> x(s, new Circle(...)); // illdefined

vector<AbstractShape*> x(s);
fill(begin(x), end(x), new Circle(...)); // illdefined

{
vector<AbstractShape*> x(s);
for (int i = 0; i != s; ++i)
    x.push_back(new Circle(...));
...
for (int i = 0; i != s; ++i)
    delete x[i];
}

class AbstractShape {...};

class Circle : public AbstractShape {...};

class Shape {
    private:
        AbstractShape* _p;

    public:
        Shape (AbstractShape* p) {
            _p = p;}

        ~Shape () {
            delete _p;}

int main () {
    {
    Shape p = new Circle(...);
    Shape q = new Triangle(...);

    Shape r = p;                 // copy constructor -> clone()

    r = q;                       // copy assignment -> clone()
    }

    vector<Circle> x(s, Circle(...));

    return 0;}

vector<AbstractShape*> x(s, new Circle(...)); // illdefined

{
vector<Shape> x(s, Shape(new Circle(...))); // Circle(...), once; Shape(...), once; Shape(Shape), s times; clone(), s times; ~Shape(), once; ~Circle(), once
}

vector<Shape> x(s, new Circle(...)); // Circle(...), once; Shape(...), once; Shape(Shape), s times; clone(), s times; ~Shape(), once; ~Circle(), once
