// -----------
// Tue, 15 Oct
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Notes
	these notes are on Canvas and GitLab
*/

/*
What did we do last time?

*/

/*
What will we do this time?

introductions
assertions
unit testing
coverage
isPrime
*/

/*
I've been here since '93
I've been at UT since '82
Full time at UT since '02, teaching OOP and SWE
*/

/*
who you are
why are taking class now
what's computer language experience
*/

/*
online judges for competitive programming
ACM started it in the 70s
ICPC independent in '18
UT has been doing since '99
I've been doing at UT since '14

competition between teams of 3 students

biweekly contests at UT
In '14 about 20 students showed up
In '19 about 300 students showed up

regional contest in Oct/Nov, usually Baylor, once at A&M
135 regions in the world
our region: 3 states, 25 schools, 65 teams

4 teams of 3 students from UT out of those 65 teams

2014: 2nd, Rice 1st, Thailand
2015: 2nd, Rice 1st, Morocco, our 4 teams were in the top 7 of the 65
2016: 1st, 2nd, 3rd, 4th, South Dakota
2017: 1st, Beijing
2018: 1st, Portugal
2019: 1st, Moscow?
*/

/*
// is the line comment
/**/ is the block comment
*/

int i = 2;
int j = 3;
int k = (i << j);

i << j; // no

k = (2 << 3);

/*
<< takes an r-value for both of its arguments
*/

i <<= j;
i <<= 2;
2 <<= 2; // no

/*
<<= takes an l-value for the left argument and an r-value for the right argument
*/

k = (i <<= j);

(i <<= j) = k;

int i = 2;
int j = 3;
int k = (i + j);
k = 2 + 3;

i + j; // no

/*
+ takes an r-value for both of its arguments
+ returns an r-value
*/

i += j;
i += 2;
2 += 2; // no

k = (i += j); // no in Python

/*
+= takes an l-value for the left argument and an r-value for the right argument
+= returns an l-value
*/

(i + j) = k; // no

(i += j) = k; // no in Python, C, Java; yes in C++

/*
overloading an operator is possible in C++, Python, not in C or Java
*/

int i;

/*
int is a type
i is a variable
i is of type int
*/

ostream cout; // somewhere in <iostream>

/*
ostream is a type
cout is a variable (global)
cout is of type ostream
*/

/*
someone overloaded << for ostream
*/

/*
what are the rules for overloading operators

1. could I pick to new token: ,,,; no
2. << is defined on int, not defined on double, could I define it on double; no
3. have to use an old token: +, -, *, <<, ...
4. have to overload an operator for a user type
5. can't change the precedence
6. can't change the associativity
7. can't change the arity (number of arguments)
8. CAN change the l-value/r-value nature of both the arguments and the return
*/

i + j * k // * has a higher precedence than +

i - j - k // - is left associative
i = j = k // = is right associtive

/*
binary  operators: *, %, =
unary   operators: ++, --, *, &
ternary operator:  ? :
n-ary   operator:  ()
*/

k = (i << j);
i << j;       // no

cout << "hi" << "bill";

cout << "\n";  // new line
cout << flush; // flush the buffer
cout << endl;  // new line and flush the buffer

/*
what does it mean for output to be buffered
*/

cout << 2;
cout << 2.3;
cout << "hi";
cout << endl; -> endl(cout)

/*
Collatz Conjecture
about 100 years old
*/

/*
take a pos int
if even, divide   by 2
if odd,  multiply by 3 and add 1
repeat until 1
*/

5 16 8 4 2 1

/*
the cycle length of  5 is 6
the cycle length of 10 is 7
*/

/*
assertions are good for preconditions (only if not related to user input), postconditions
they're NOT good for testing    (Google Test)
they're NOT good for user input (exceptions)
*/

/*
broken tests can hide broken code
*/

/*
1. run as is, confirm success
2. identify, keep track of, and fix the broken tests
3. run again, confirm failure
4. identify and fix the code
5. run again, confirm success
*/

/*
any e-mail address
the code is 1234
*/

/*
oopl.com/ni
*/

bool is_prime (int n) {
    assert(n > 0);
    if ((n == 1) || ((n % 2) == 0))
        return false;
    for (int i = 3; i < std::sqrt(n) + 1; ++++i)
        if ((n % i) == 0)
            return false;
    return true;}

int i = 2;
int j = ++i;
cout << i;   // 3
cout << j;   // 3

int i = 2;
int j = i++;
cout << i;   // 3
cout << j;   // 2

f(++i)
f(i++)

++i;
i++;

I p = ...;
++p;
p++;

++2; // no

++i = j;

/*
pre increment needs an l-value and returns an l-value
*/

2++; // no

/*
post increment needs an l-value and returns an r-value
*/

i++ = j; // no

++++i;

i++++; // no
