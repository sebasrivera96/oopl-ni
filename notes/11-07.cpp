// -----------
// Thu,  7 Nov
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Notes
	these notes are on Canvas and GitLab
*/

/*
What did we do last time?

range iterator
*/

/*
What will we do this time?

range
implicit type conversion
symmetric operators
friends
compiler optimizations
*/

A x = 2;
A x(2);
A x{2};
A x = {2};
A x = A(2);

x + y -> x.operator+(y)
x - y -> x.operator-(y)
x = y -> x.operator=(y)

A x = y;
A x(y);
A x{y};
A x = {y};

struct A {
    A () {
        cout << "A() ";}

    A (int) {
        cout << "A(int) ";}

    A (const A&) {
        cout << "A(A) ";}

    A& operator = (const A&) {
        cout << "=(A) ";
        return *this;}

    ~A () {
        cout << "~A() ";}};

void f (A y) {
    ...}

int main () {
    A x(...);
    f(x);
    return 0;}

x == y -> x.operator==(y)

struct A {
    void f () {
        // A* const this
    }

    void g () const {
        // const A* const this
    }
    };

int main () {
    A x(...);
    x.f();     // this -> x
    return 0;}

/*
input iterator
* (read only)
++ (pre and post)
==, !=
*/

RangeIterator (const T& v) : // much better
        _v (v)               // T(T)
    {}

RangeIterator (const T& v) { // T()
    _v = v;}                 // =(T)

/* provided for free
A(), unless any other constructor is defined
A(A)
=(A)
~A()
*/

struct A {
    A (int) {}

    bool operator == (const A& rhs) const {
        ...}};

void f (A y) {}

int main () {
    A z;      // no
    A x = 2;

    f(x);
    f(A(2));
    f(2);

    A y = 3;

    cout << (x == y);
    cout << x.operator==(y);

    cout << (x == 2);
    cout << x.operator==(2);

    cout << (2 == x);        // no
    cout << 2.

    return 0;}

class A {
    friend bool operator == (const A&, const A&);

    private:
        int _v;

    public:
        A (int v) :
            _v (v)
            {}

bool operator == (const A& lhs, const A& rhs) {
    return lhs._v == rhs._v;}

void f (A y) {}

int main () {
    A z;      // no
    A x = 2;

    f(x);
    f(A(2));  // A(int), A(A), ~A() -> A(int)
    f(2);

    A y = 3;

    cout << (x == y);
    cout << operator==(x, y);

    cout << (x == 2);
    cout << operator==(x, 2);

    cout << (2 == x);
    cout << operator==(2, x);

    return 0;}

const RangeIterator<int> b = 2;
const RangeIterator<int> e = 2;

cout << (b == e);
cout << (b == 2);
cout << (2 == b); // no

/*
C++ has the notion of friend

1. a class is the entity that can make the declartion
2. the target of the friendship can be a class, a method, or a function
3. a friend has access to everything
4. friendship is directed, the declarer is the grantor of the access
5. friendship is not transitive
6. friendship is not symmetric
*/

class A {
    friend bool operator == (const A& lhs, const A& rhs) {
        return lhs._v == rhs._v;}

    private:
        int _v;

    public:
        A (int v) :
            _v (v)
            {}


const Range<int> x(2, 2);
const Range<int> x{2, 2};
const Range<int> x = {2, 2};

template <typename T>
class Range1 {
    private:
        RangeIterator<T> _b;
        RangeIterator<T> _e;
    public:
        Range (const T& b, const T& e) :
            _b (b),                        // RI(T)
            _e (e)                         // RI(T)
            {}
        RangeIterator<T> begin () const {
            return _b;}                    // RI(RI)
        RangeIterator<T> end () const {
            return _e;}                    // RI(RI)

template <typename T>
class Range2 {
    private:
        T _b;
        T _e;
    public:
        Range (const T& b, const T& e) :
            _b (b),                        // T(T)
            _e (e)                         // T(T)
            {}
        RangeIterator<T> begin () const {
            return _b;}                    // RI(T)
        RangeIterator<T> end () const {
            return _e;}                    // RI(T)

class string {
    private:
        ...
    public:
        string (const char* a) {
            ...}};

void f (string t) {
    ...}

int main () {
    string s = "abc";
    string s("abc");
    string s{"abc"};
    string s = {"abc"}
    f(s);
    f("abc");         // string(char*), string(string), ~string() -> string(char*)
    return 0;}

template<typename T>
class vector {
    private:
        ...
    public:
        explicit vector (int s) {
            ...}};

void f (vector<int> y) {
    ...}

int main () {
    vector<int> x(10);
    vecotor<int> x = 10; // no
    f(x);                // vector(vector)
    f(10);               // no
    return 0;}
