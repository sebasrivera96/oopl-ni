// -----------
// Tue, 19 Nov
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Notes
	these notes are on Canvas and GitLab
*/

/*
What did we do last time?

initialization
initizlizer_list
vector
*/

/*
What will we do this time?

copy constructor
copy assignment
equality
r-value reference
*/

(i = j) = k;

struct A {};

/*
A(), if you don't define ANY other constructor
A(A)
=(A)
~A()
*/

void f (vector<int> y) {
    ...}

int main () {
    vector<int> x(2);
    f(x);
    f(2);             // no

    vector<int> x{2};
    f(x);
    f({2});           // yes
    return 0;}

template <typename T>
class my_vector {
    friend bool operator == (const my_vector& lhs, const my_vector& rhs) {
        return (lhs.size() == rhs.size()) && equal(begin(lhs), end(lhs), begin(rhs));}

    private:
        pointer _b = nullptr;
        pointer _e = nullptr;

    public:
//        my_vector (const my_vector& rhs) {
//            _b = rhs._b;
//            _e = rhs._e;}

//        my_vector& operator = (const my_vector& rhs) {
//            _b = rhs._b;
//            _e = rhs._e;
//            return *this;

        my_vector (const my_vector& rhs) {
 //           *this = rhs;
            if (!rhs.size()) {
                _b = new T[rhs.size()];
                _e = _b + rhs.size();
                copy(begin(rhs), end(rhs), _b);}

        my_vector& operator = (my_vector rhs) {
            swap(_b, rhs._b);
            swap(_e, rhs._e);
 //           my_vector tmp(*this);
 /*
            if (this == &rhs)
                return *this;
            delete [] _b;
            if (!rhs.size()) {
                _b = new T[rhs.size()];
                _e = _b + rhs.size();
                copy(begin(rhs), end(rhs), _b);}
            else
                _b = _e = nullptr;
*/
            return *this;

void f (vector<int> z) {
    ...}

int main () {
    vector<int> x(10, 2);
    vector<int> y(20, 3);
    x = y;
    x.operator=(y);
    return 0;}

vector<int> f (...) {
    return vector<int>(10, 2);}

int main () {
    vector<int> y = f(...);
    return 0;}

vector<int> x(10, 2);
vector<int> y(20, 3);
swap(x, y);           // O(i)
