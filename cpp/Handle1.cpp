// -----------
// Handle1.c++
// -----------

#include <algorithm> // swap
#include <cassert>   // assert
#include <iostream>  // cout, endl, istream, ostream
#include <utility>   // !=

using namespace std;
using rel_ops::operator!=;

void test0 () {
    const Shape x = new Circle(2, 3, 4);
//  x.move(5, 6);                        // doesn't compile
    assert(x.area() == 3.14 * 4 * 4);
//  x.radius();                          // doesn't compile
    }

void test1 () {
    Shape x = new Circle(2, 3, 4);
    x.move(5, 6);
    assert(x        == new Circle(5, 6, 4));
    assert(x.area() == 3.14 * 4 * 4);
//  x.radius();                        // doesn't compile
    }

void test2 () {
    const Shape x = new Circle(2, 3, 4);
    Shape       y = x;
    assert(x == y);
    y.move(5, 6);
    assert(x        == new Circle(2, 3, 4));
    assert(y        == new Circle(5, 6, 4));
    assert(y.area() == 3.14 * 4 * 4);}

void test3 () {
    const Shape x = new Circle(2, 3, 4);
    Shape       y = new Circle(2, 3, 5);
    assert(x != y);
    y = x;
    assert(x == y);
    y.move(5, 6);
    assert(x        == new Circle(2, 3, 4));
    assert(y        == new Circle(5, 6, 4));
    assert(y.area() == 3.14 * 4 * 4);}

void test4 () {
    const Shape x;
    const Shape y = x;
    assert(x == y);}

void test5 () {
    const Shape x;
    Shape       y = new Circle(2, 3, 5);
    assert(x != y);
    y = x;
    assert(x == y);}

int main () {
    cout << "Handle1.c++" << endl;
    test0();
    test1();
    test2();
    test3();
    test4();
    test5();
    cout << "Done." << endl;
    return 0;}
