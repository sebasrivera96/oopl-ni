// --------------
// TransformT.c++
// --------------

// https://en.cppreference.com/w/cpp/algorithm/transform

#include <cstdio>

#include <algorithm>  // transform, equal
#include <functional> // function
#include <list>       // list
#include <vector>     // vector

#include "gtest/gtest.h"

using namespace std;
using namespace testing;

template <typename II, typename OI, typename UF>
OI my_transform (II b, II e, OI x, UF f) {
    while (b != e) {
        *x = f(*b);
        ++b;
        ++x;}
    return x;}

using TransformArraySignature = function<int*                  (const int*,                const int*,                int*,                  function<int (int)>)>;
using TransformListSignature  = function<vector<int>::iterator (list<int>::const_iterator, list<int>::const_iterator, vector<int>::iterator, function<int (int)>)>;

struct TransformArrayFixture : TestWithParam<TransformArraySignature>
    {};

struct TransformListFixture : TestWithParam<TransformListSignature>
    {};

INSTANTIATE_TEST_CASE_P(
    TransformArrayInstantiation,
    TransformArrayFixture,
    Values(
           transform<const int*, int*, function<int (int)>>,
        my_transform<const int*, int*, function<int (int)>>),);

INSTANTIATE_TEST_CASE_P(
    TransformListInstantiation,
    TransformListFixture,
    Values(
           transform<list<int>::const_iterator, vector<int>::iterator, function<int (int)>>,
        my_transform<list<int>::const_iterator, vector<int>::iterator, function<int (int)>>),);

TEST_P(TransformArrayFixture, test0) {
    const int  ia[]  = {2, 3, 4};
    int        ob[5] = {};
    const int* b     = begin(ia);
    const int* e     = end(ia);
    int*       x     = begin(ob) + 1;
    auto       f     = [] (int v) -> int {return v * v;};
    const int* p     = GetParam()(b, e, x, f);
    ASSERT_EQ(p, begin(ob) + 4);
    ASSERT_TRUE(equal(begin(ob), end(ob), begin({0, 4, 9, 16, 0})));}

TEST_P(TransformListFixture, test1) {
    const list<int>           ix = {2, 3, 4};
    vector<int>               oy(5);
    list<int>::const_iterator b = begin(ix);
    list<int>::const_iterator e = end(ix);
    vector<int>::iterator     x = begin(oy) + 1;
    auto                      f = [] (int v) -> int {return v * v;};
    vector<int>::iterator     p = GetParam()(b, e, x, f);
    ASSERT_EQ(p, begin(oy) + 4);
    ASSERT_TRUE(equal(begin(oy), end(oy), begin({0, 4, 9, 16, 0})));}
