// ---------
// Equal.c++
// ---------

// https://en.cppreference.com/w/cpp/algorithm/equal

#include <cstdio>

#include <algorithm> // equal
#include <cassert>   // assert
#include <iostream>  // cout, endl
#include <list>      // list
#include <vector>    // vector

using namespace std;

void test0 () {
    const int  x[] = {2, 3, 4};
    const int  y[] = {0, 2, 3, 4, 0};
    const int* b   = begin(x);
    const int* e   = end(x);
    const int* c   = begin(y);
    assert(!equal(b, e, c));}

void test1 () {
    const int  x[] = {2, 3, 4};
    const int  y[] = {0, 2, 3, 4, 0};
    const int* b   = begin(x);
    const int* e   = end(x);
    const int* c   = begin(y) + 1;
    assert(equal(b, e, c));}

void test2 () {
    const int  x[] = {2, 3, 4};
    const int  y[] = {0, 2, 3, 4, 0};
    const int* b   = end(x);
    const int* e   = end(x);
    const int* c   = end(y);
    assert(equal(b, e, c));}

void test3 () {
    const list<int>             x = {2, 3, 4};
    const vector<int>           y = {0, 2, 3, 4, 0};
    list<int>::const_iterator   b = begin(x);
    list<int>::const_iterator   e = end(x);
    vector<int>::const_iterator c = begin(y) + 1;
    assert(equal(b, e, c));}

int main () {
    cout << "Equal.c++" << endl;
    test0();
    test1();
    test2();
    test3();
    cout << "Done." << endl;
    return 0;}
