// ------------
// Handle3T.c++
// ------------

#include <algorithm> // swap
#include <cassert>   // assert
#include <iostream>  // istream, ostream
#include <utility>   // !=

#include "gtest/gtest.h"

using namespace std;
using rel_ops::operator!=;

class AbstractShape {
    friend bool operator == (const AbstractShape& lhs, const AbstractShape& rhs) {
        return lhs.equals(rhs);}

    friend istream& operator >> (istream& lhs, AbstractShape& rhs) {
        return rhs.read(lhs);}

    friend ostream& operator << (ostream& lhs, const AbstractShape& rhs) {
        return rhs.write(lhs);}

    private:
        int _x;
        int _y;

    protected:
        AbstractShape& operator = (const AbstractShape&) = default;

        virtual bool     equals (const AbstractShape& rhs) const = 0;
        virtual istream& read   (istream&             in)        = 0;
        virtual ostream& write  (ostream&             out) const = 0;

    public:
        AbstractShape (int x, int y) :
                _x (x),
                _y (y)
            {}

        AbstractShape          (const AbstractShape&) = default;
        virtual ~AbstractShape ()                     = default;

        virtual double         area  () const = 0;
        virtual AbstractShape* clone () const = 0;

        void move (int x, int y) {
            _x = x;
            _y = y;}};

bool AbstractShape::equals (const AbstractShape& rhs) const {
    return (_x == rhs._x) && (_y == rhs._y);}

istream& AbstractShape::read (istream& in) {
    return in >> _x >> _y;}

ostream& AbstractShape::write (ostream& out) const {
    return out << _x << " " << _y;}

class Circle : public AbstractShape {
    friend bool operator == (const Circle& lhs, const Circle& rhs) {
        return lhs.AbstractShape::equals(rhs) && (lhs._r == rhs._r);}

    friend istream& operator >> (istream& lhs, Circle& rhs) {
        return rhs.read(lhs);}

    friend ostream& operator << (ostream& lhs, const Circle& rhs) {
        return rhs.write(lhs);}

    private:
        int _r;

    protected:
        bool equals (const AbstractShape& rhs) const override {
            if (const Circle* const p = dynamic_cast<const Circle*>(&rhs))
                return AbstractShape::equals(rhs) && (_r == p->_r);
            return false;}

        istream& read (istream& in) override {
            return AbstractShape::read(in) >> _r;}

        ostream& write (ostream& out) const override {
            return AbstractShape::write(out) << " " << _r;}

    public:
        Circle (int x, int y, int r) :
                AbstractShape (x, y),
                _r            (r)
            {}

        Circle             (const Circle&) = default;
        ~Circle            ()              = default;
        Circle& operator = (const Circle&) = default;

        double area () const override {
            return 3.14 * _r * _r;}

        Circle* clone () const override {
            return new Circle(*this);}

        int radius () const {
            return _r;}};

template <typename T>
class Handle {
    friend bool operator == (const Handle& lhs, const Handle& rhs) {
        if ((lhs.get() == nullptr) && (rhs.get() == nullptr))
            return true;
        if ((lhs.get() == nullptr) || (rhs.get() == nullptr))
            return false;
        return (*lhs.get() == *rhs.get());}

    public:
        typedef T                 value_type;

        typedef value_type*       pointer;
        typedef const value_type* const_pointer;

        typedef value_type&       reference;
        typedef const value_type& const_reference;

    private:
        struct count {
            int     _n = 0;
            pointer _p = nullptr;

            count (pointer p) {
                assert(p != nullptr);
                _n = 1;
                _p = p;}

            count             (const count&) = delete;
            count& operator = (const count&) = delete;

            ~count () {
                assert(_n == 1);
                delete _p;}};

        count* _c = nullptr;

    protected:
        pointer get () {
            if (_c == nullptr)
                return nullptr;
            if (!unique()) {
                --_c->_n;
                _c = new count(_c->_p->clone());}
            return _c->_p;}

        const_pointer get () const {
            if (_c == nullptr)
                return nullptr;
            return _c->_p;}

    public:
        Handle () = default;

        Handle (pointer p) {
            if (p != nullptr)
                _c = new count(p);}

        Handle (const Handle& rhs) {
            if (rhs._c != nullptr) {
                _c = rhs._c;
                ++_c->_n;}}

        Handle& operator = (const Handle& rhs) {
            if (this == &rhs)
                return *this;
            Handle that(rhs);
            swap(that);
            return *this;}

        ~Handle () {
            if (unique())
                delete _c;
            else
                --_c->_n;}

        void swap (Handle& that) {
            std::swap(_c, that._c);}

        bool unique () const {
            return use_count() <= 1;}

        int use_count () const {
            if (_c != nullptr)
                return _c->_n;
            return 0;}};

struct Shape : Handle<AbstractShape> {
    Shape () = default;

    Shape (AbstractShape* p) :
            Handle<AbstractShape> (p)
        {}

    Shape             (const Shape&) = default;
    ~Shape            ()             = default;
    Shape& operator = (const Shape&) = default;

    double area () const {
        assert(get() != nullptr);
        return get()->area();}

    void move (int x, int y) {
        assert(get() != nullptr);
        return get()->move(x, y);}};

TEST(HandleFixture, test0) {
    const Shape x = new Circle(2, 3, 4);
//  x.move(5, 6);                        // doesn't compile
    ASSERT_EQ(x.area(), 3.14 * 4 * 4);
//  x.radius();                          // doesn't compile
    ASSERT_TRUE(x.unique());
    ASSERT_EQ(x.use_count(), 1);}

TEST(HandleFixture, test1) {
    Shape x = new Circle(2, 3, 4);
    x.move(5, 6);
    ASSERT_EQ(x,        new Circle(5, 6, 4));
    ASSERT_EQ(x.area(), 3.14 * 4 * 4);
//  x.radius();                               // doesn't compile
    ASSERT_TRUE(x.unique());
    ASSERT_EQ(x.use_count(), 1);}

TEST(HandleFixture, test2) {
    const Shape x = new Circle(2, 3, 4);
    Shape       y = x;
    ASSERT_EQ(x, y);
    ASSERT_FALSE(x.unique());
    ASSERT_FALSE(y.unique());
    ASSERT_EQ(x.use_count(), 2);
    ASSERT_EQ(y.use_count(), 2);
    y.move(5, 6);
    ASSERT_EQ(x,        new Circle(2, 3, 4));
    ASSERT_EQ(y,        new Circle(5, 6, 4));
    ASSERT_EQ(y.area(), 3.14 * 4 * 4);
    ASSERT_TRUE(x.unique());
    ASSERT_TRUE(y.unique());
    ASSERT_EQ(x.use_count(), 1);
    ASSERT_EQ(y.use_count(), 1);}

TEST(HandleFixture, test3) {
    const Shape x = new Circle(2, 3, 4);
    Shape       y = new Circle(2, 3, 5);
    ASSERT_NE(x, y);
    y = x;
    ASSERT_EQ(x, y);
    ASSERT_FALSE(x.unique());
    ASSERT_FALSE(y.unique());
    ASSERT_EQ(x.use_count(), 2);
    ASSERT_EQ(y.use_count(), 2);
    y.move(5, 6);
    ASSERT_EQ(x,        new Circle(2, 3, 4));
    ASSERT_EQ(y,        new Circle(5, 6, 4));
    ASSERT_EQ(y.area(), 3.14 * 4 * 4);
    ASSERT_TRUE(x.unique());
    ASSERT_TRUE(y.unique());
    ASSERT_EQ(x.use_count(), 1);
    ASSERT_EQ(y.use_count(), 1);}

TEST(HandleFixture, test4) {
    const Shape x;
    const Shape y = x;
    ASSERT_EQ(x, y);}

TEST(HandleFixture, test5) {
    const Shape x;
    Shape       y = new Circle(2, 3, 5);
    ASSERT_NE(x, y);
    y = x;
    ASSERT_EQ(x, y);}
