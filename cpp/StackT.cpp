// ----------
// StackT.c++
// ----------

// http://en.cppreference.com/w/cpp/container/stack

#include <cstddef> // size_t
#include <deque>   // deque
#include <stack>   // stack
#include <vector>  // vector

#include "gtest/gtest.h"

using namespace std;
using namespace rel_ops;

using namespace testing;

template <typename T, typename C = std::deque<T>>
class my_stack {
    friend bool operator == (const my_stack& lhs, const my_stack& rhs) {
        return lhs._c == rhs._c;}

    public:
        using container_type  = C;

        using value_type      = typename container_type::value_type;
        using size_type       = typename container_type::size_type;

        using reference       = typename container_type::reference;
        using const_reference = typename container_type::const_reference;

    private:
        container_type _c;

    public:
        my_stack             ()                 = default;
        my_stack             (const my_stack&)  = default;
        ~my_stack            ()                 = default;
        my_stack& operator = (const my_stack&)  = default;

        explicit my_stack (const container_type& c) :
                _c (c)
            {}

        bool empty () const {
            return _c.empty();}

        void pop () {
            _c.pop_back();}

        void push (const_reference v) {
            _c.push_back(v);}

        size_type size () const {
            return _c.size();}

        reference top () {
            return _c.back();}

        const_reference top () const {
            return _c.back();}};

template <typename T>
struct StackFixture : Test {
    using stack_type = T;};

using
    stack_types =
    Types<
           stack<int>,
           stack<int, vector<int>>,
        my_stack<int>,
        my_stack<int, vector<int>>>;

#ifdef __APPLE__
    TYPED_TEST_CASE(StackFixture, stack_types,);
#else
    TYPED_TEST_CASE(StackFixture, stack_types);
#endif

TYPED_TEST(StackFixture, test0) {
    using stack_type = typename TestFixture::stack_type;
    stack_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), size_t(0));}

TYPED_TEST(StackFixture, test1) {
    using stack_type = typename TestFixture::stack_type;
    stack_type x;
    x.push(2);
    x.push(3);
    x.push(4);
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), size_t(3));
    ASSERT_EQ(x.top(),  4);}

TYPED_TEST(StackFixture, test2) {
    using stack_type = typename TestFixture::stack_type;
    stack_type x;
    x.push(2);
    x.push(3);
    x.push(4);
    x.pop();
    ASSERT_EQ(x.size(), size_t(2));
    ASSERT_EQ(x.top(),  3);}

TYPED_TEST(StackFixture, test3) {
    using stack_type = typename TestFixture::stack_type;
    stack_type x;
    x.push(2);
    x.push(3);
    x.push(4);
    x.top() = 5;
    ASSERT_EQ(x.top(), 5);
    const stack_type& r = x;
//  r.top() = 6;             // error: assignment of read-only location '(& r)->std::stack_type::top()'
    ASSERT_EQ(r.top(), 5);}

TYPED_TEST(StackFixture, test4) {
    using stack_type = typename TestFixture::stack_type;
    stack_type x;
    x.push(2);
    x.push(3);
    x.push(4);
    stack_type y = x;
    ASSERT_EQ(x, y);}

TYPED_TEST(StackFixture, test5) {
    using stack_type = typename TestFixture::stack_type;
    stack_type x;
    x.push(2);
    x.push(3);
    x.push(4);
    stack_type y;
    x.push(5);
    x.push(6);
    stack_type& z = (x = y);
    ASSERT_EQ(x,  y);
    ASSERT_EQ(&x, &z);}
