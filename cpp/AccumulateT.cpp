// ---------------
// AccumulateT.c++
// ---------------

// https://en.cppreference.com/w/cpp/algorithm/accumulate

#include <functional> // function, minus, multiplies, plus
#include <iostream>   // cout, endl
#include <numeric>    // accumulate
#include <list>       // list
#include <vector>     // vector

#include "gtest/gtest.h"

using namespace std;
using namespace testing;

template <typename II, typename T, typename BF>
T my_accumulate (II b, II e, T v, BF f) {
    while (b != e) {
        v = f(v, *b);
        ++b;}
    return v;}

using AccumulateArraySignature = function<int (const int*,                const int*,                int, function<int (int, int)>)>;
using AccumulateListSignature  = function<int (list<int>::const_iterator, list<int>::const_iterator, int, function<int (int, int)>)>;

struct AccumulateArrayFixture : TestWithParam<AccumulateArraySignature>
    {};

struct AccumulateListFixture : TestWithParam<AccumulateListSignature>
    {};

INSTANTIATE_TEST_CASE_P(
    AccumulateArrayInstantiation,
    AccumulateArrayFixture,
    Values(
           accumulate<const int*, int, function<int (int, int)>>,
        my_accumulate<const int*, int, function<int (int, int)>>),);

INSTANTIATE_TEST_CASE_P(
    AccumulateListInstantiation,
    AccumulateListFixture,
    Values(
           accumulate<list<int>::const_iterator, int, function<int (int, int)>>,
        my_accumulate<list<int>::const_iterator, int, function<int (int, int)>>),);

TEST_P(AccumulateArrayFixture, test0) {
    const int  a[] = {2, 3, 4};
    const int* b   = begin(a);
    const int* e   = end(a);
    const int  v   = 0;
    auto       f   = plus<int>();
    ASSERT_EQ(GetParam()(b, e, v, f), 9);}

TEST_P(AccumulateListFixture, test1) {
    const list<int>           x = {2, 3, 4};
    list<int>::const_iterator b = begin(x);
    list<int>::const_iterator e = end(x);
    const int                 v   = 1;
    auto                      f = multiplies<int>();
    ASSERT_EQ(GetParam()(b, e, v, f), 24);}
